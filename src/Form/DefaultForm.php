<?php

namespace Drupal\shareaholic_simple\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DefaultForm.
 *
 * @package Drupal\shareaholic_simple\Form
 */
class DefaultForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'shareaholic_simple.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('shareaholic_simple.default');
    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#description' => $this->t('On https://shareaholic.com please add your site and after creating get Site ID on https://shareaholic.com/publisher_tools.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('site_id'),
    ];
    $form['app_id'] = [
      '#type' => 'number',
      '#title' => $this->t('App ID'),
      '#description' => $this->t('On https://shareaholic.com/publisher_tools page in Inline Apps code snippet get "data-app-id" attribute.'),
      '#default_value' => $config->get('app_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('shareaholic_simple.default')
      ->set('site_id', $form_state->getValue('site_id'))
      ->set('app_id', $form_state->getValue('app_id'))
      ->save();
  }

}
